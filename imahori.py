import networkx as nx
import matplotlib.pyplot as plt
import random

g = nx.DiGraph()

g.add_node('s')
g.add_node('u')
g.add_node('v')
g.add_node('w')
g.add_node('x')
g.add_node('t')


g.add_edge('s', 'u', capacity=20, flow=5)
g.add_edge('s', 'w', capacity=20, flow=10)
g.add_edge('u', 'v', capacity=5, flow=0)
g.add_edge('u', 'w', capacity=15, flow=5)
g.add_edge('v', 'w', capacity=10, flow=0)
g.add_edge('w', 'x', capacity=25, flow=15)
g.add_edge('x', 'v', capacity=5, flow=5)
g.add_edge('x', 't', capacity=10, flow=10)
g.add_edge('v', 't', capacity=15, flow=5)

start = 's'
goal = 't'

pre_mark = []
choice = [start]
mark = [start]
apass_dic = {}
flow_dic = {}

while True: 
	for n in g.successors(start):
		apass = g.edge[start][n]["capacity"] - g.edge[start][n]["flow"]
		if apass != 0:
			apass_dic[mark[0], n] = apass
			pre_mark.append(n)
			g.node[n]["marked"] = start

	a = random.choice(pre_mark)
	mark.extend(pre_mark)
	choice.append(a)

	while goal not in mark:
		del pre_mark[:]
		for n in g.successors(a):
			if mark.count(n) == 0:
				apass = g.edge[a][n]["capacity"] - g.edge[a][n]["flow"]
				if apass != 0:
					apass_dic[a, n] = apass
					pre_mark.append(n)
					g.node[n]["marked"] = a
		for n in g.predecessors(a):
			if mark.count(n) == 0 and goal not in mark:
				flow = g.edge[n][a]["flow"]
				if flow != 0:
					flow_dic[a, n] = flow
					pre_mark.append(n)
					g.node[n]["marked"] = a
		if pre_mark:
			if goal not in mark:
				a = random.choice(pre_mark)
				mark.extend(pre_mark)
				choice.append(a)	
				pre_mark.append(n)
			elif goal in mark:
				choice.append(goal)
		else:
			for n in mark:
				if n not in choice:
					a = n
					choice.append(a)

	zpath_dic = {}
	gpath_dic = {}

	mini = 100
	t = goal

	while start in choice:
		if t == start:
			break
		else:
			c = g.node[t]["marked"]
			tem_edge = (t, c)
		if tem_edge in g.edges():
			pass
		else:
			tem_edge = (c, t)
		if t in g.successors(c):
			apass = g.edge[c][t]["capacity"] - g.edge[c][t]["flow"]
			a = apass
			zpath_dic[tem_edge] = apass
		else:
			flow = g.edge[t][c]["flow"]
			a = flow
			gpath_dic[tem_edge] = flow
		if mini > a:
			mini = a
		else:
			pass
		t = g.node[t]["marked"]

	s = 0

	for key in zpath_dic.keys():
		if mini <= zpath_dic[key]:
			floow = zpath_dic[key] + mini
			zpath_dic[key] = floow
			s = floow
			caapa = g.edge[key[0]][key[1]]["capacity"]
		else:
			break
	for keyf in gpath_dic.keys():
		floow = gpath_dic[keyf] - mini
		gpath_dic[keyf] = floow
		if 0 <= floow:
			key = gpath_dic[keyf]
		else:
			floow = gpath_dic[keyf] + mini
			gpath_dic[keyf] = floow
			break

	print "max flow :", s
	break	


nx.draw(g)
plt.show()

