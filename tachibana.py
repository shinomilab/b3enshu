# -*- coding:utf-8 -*-
import networkx as nx
from networkx.algorithms import bipartite
import matplotlib.pyplot as plt
import random as rm
from numpy.random import *

N=5
MAN=range(N)
WOMAN=range(N,2*N)
B = nx.Graph()
B.add_nodes_from(MAN, bipartite=0,pointer=0)
B.add_nodes_from(WOMAN, bipartite=1)
top_nodes = set(n for n,d in B.nodes(data=True) if d['bipartite']==0)
bottom_nodes = set(B) - top_nodes
S_MAN=list(top_nodes)
S_WOMAN=list(bottom_nodes)
M_MAN=[]
M_WOMAN=[]

####################　各ノードにリストを渡す　#################### 開始
B.node[0]['List']=list(S_WOMAN)
for i in range(1,N):
	TEMP1=B.node[i-1]['List']
	rm.shuffle(TEMP1)
	B.node[i]['List']=list(TEMP1)#リストをもたせる
B.node[N]['List']=list(S_MAN)
for j in range(N+1,2*N):
	TEMP2=B.node[j-1]['List']
	rm.shuffle(TEMP2)
	B.node[j]['List']=list(TEMP2)#リストをもたせる
####################　各ノードにリストを渡す　#################### 終了

total_propose=0#プロポーズ回数カウント用変数

print "===============INITIAL STATE==============="
for node in B.nodes():
	print str(node)+str(" has LIST:")+str(B.node[node]['List'])
print "S_MAN   = "+str(S_MAN)
print "S_WOMAN = "+str(S_WOMAN)
print "M_MAN   = "+str(M_MAN)
print "M_WOMAN = "+str(M_WOMAN)
print "Edges = " +str(B.edges())
#print B.node
print "==========================================="

while 1:
	for MAN in S_MAN:
		total_propose+=1
		P=B.node[MAN]['pointer']#次にプロポーズする女性の順位
		temp_X=MAN
		temp_Y=B.node[MAN]['List'][P]
		print "========================================"
		print str("[")+str(temp_X)+str("]は,リストの[")+str(P)+str("]番目の[")+str(temp_Y)+str("]にプロポーズする")

		if temp_Y in S_WOMAN:
			print str("[")+str(temp_Y)+str("]は,独身なので婚約する")
			B.add_edge(temp_X,temp_Y)
			S_WOMAN.remove(temp_Y)
			M_WOMAN.append(temp_Y)
			S_MAN.remove(temp_X)
			M_MAN.append(temp_X)

		elif temp_Y in M_WOMAN:
			temp_Z=B.neighbors(temp_Y)[0]
			if B.node[temp_Y]['List'].index(temp_X) < B.node[temp_Y]['List'].index(temp_Z):
				print str("[")+str(temp_Y)+str("]は,[")+str(temp_Z)+str("]と婚約していたが,[")+str(temp_X)+str("]のほうが好きなので,[")+str(temp_X)+str("]と婚約")
				M_MAN.remove(temp_Z)
				S_MAN.append(temp_Z)
				B.remove_edge(B.neighbors(B.node[MAN]['List'][P])[0],B.node[MAN]['List'][P])
				B.add_edge(MAN,B.node[MAN]['List'][P])
				S_MAN.remove(temp_X)
				M_MAN.append(temp_X)
			else:
				print str("[")+str(MAN)+str("]は,[")+str(temp_Y)+str("]にプロポーズしたが,ふられた")
		else:
			print str("[")+str(MAN)+str("]は,[")+str(temp_Y)+str("]にプロポーズしたが,ふられた")

		print "S_MAN   = " + str(S_MAN)
		print "S_WOMAN = " + str(S_WOMAN)
		print "M_MAN   = " + str(M_MAN)
		print "M_WOMAN = " + str(M_WOMAN)
		print "Edges = " + str(B.edges())

		B.node[MAN]['pointer']+=1
	if S_MAN==[]:
		break

print "================FINAL OUTPUT================"
print str("Total Propose:")+str(total_propose)
print str("Bipartite:")+str(bipartite.is_bipartite(B))
print "S_MAN   = "+str(S_MAN)
print "S_WOMAN = "+str(S_WOMAN)
print "M_MAN   = "+str(M_MAN)
print "M_WOMAN = "+str(M_WOMAN)
print "Edges = " +str(B.edges())
print B.node
print "============================================"

pos=nx.spring_layout(B) # positions for all nodes

##_nodes
nx.draw_networkx_nodes(B,pos,nodelist=M_MAN,node_color='b',node_size=700,alpha=0.9)
nx.draw_networkx_nodes(B,pos,nodelist=M_WOMAN,node_color='r',node_size=700,alpha=0.9)

##_edges
nx.draw_networkx_edges(B,pos,edgelist=B.edges(),width=4,alpha=0.9,edge_color='g')
##_some math labels
labels={}
for L in B.nodes():
	labels[L]=L
nx.draw_networkx_labels(B,pos,labels,font_size=16)

plt.axis('off')
plt.show()

