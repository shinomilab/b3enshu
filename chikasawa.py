import networkx as nx
import matplotlib.pyplot as plt

g = nx.newman_watts_strogatz_graph(30,2,0.5)
removable = []
dec = []

for node in g.nodes():
	removable.append(node)

while removable != []:
	node = removable[0]
	if node in removable:
		removable.remove(node)
	for neighbor in nx.all_neighbors(g,node):
		if neighbor in removable:
			removable.remove(neighbor)
			dec.append(neighbor)
print len(dec)

pos = nx.circular_layout(g)

nx.draw_networkx_nodes(g, pos, nodelist = g.nodes(), node_color = 'w', node_size = 300, alpha = 1)
nx.draw_networkx_nodes(g, pos, nodelist = dec, node_color = 'r', node_size = 300, alpha = 1)
nx.draw_networkx_edges(g, pos, g.edges(), width = 1.5, alpha = 1, edge_color = 'k')

plt.axis('off')
plt.show()


