# -*- coding: utf-8 -*-

import networkx as nx #networkxをnxと名づけて利用する
import matplotlib.pyplot as plt
import random

g = nx.Graph()

for i in range(0,7):
        g.add_node(i)

g.add_edge(0,1)
g.add_edge(0,4)
g.add_edge(1,2)
g.add_edge(1,3)
g.add_edge(1,5)
g.add_edge(2,3)
g.add_edge(2,6)
g.add_edge(3,4)
g.add_edge(4,5)
g.add_edge(4,6)


clist=["C"+"("+str(x)+")" for x in range(7)] #彩色用カラーリスト

nlist=g.nodes() #頂点リスト
print nlist

elist=g.edges() #弧リスト
print elist

print clist
#カラーリスト


dict1 = {}
lenlist = dict1.values() #d(Pi)の値を格納するリスト

for i in range(0,7):
        neighlist = g.neighbors(i)
        print "P"+str(i) + " : "+ str(neighlist) #Piに隣接しているノードを表示
        print "P"+str(i) + " : "+ str(len(neighlist)) #Piに隣接しているノード数を表示

        dict1[i] = len(neighlist)
        lenlist.append(len(neighlist))

print dict1
keylist = [] #dictのkeyを格納するリスト
for k, v in sorted(dict1.items(), key=lambda x:x[1]): #valueの値でdictをソート
        keylist.append(k) #keylistにkeyの数字を格納
        #print k, v

lenlist.sort() #小さい順に次数を並べかえる
lenlist.reverse() #それを逆に並べ替える

print lenlist
keylist.reverse() #逆に並べ替える
print keylist

dict2={}
main_keylist=list(keylist)

n_lenlist=dict1.values()
#print n_lenlist

for c_num in range(0,100):
        dict2[main_keylist[0]]=clist[c_num]
        n_num = int(n_lenlist[main_keylist[0]]) #int型になおす
        print n_num
        key0_neighbors=list(g.neighbors(n_num)) #keylist[0]に格納されている数字のノードに隣接するノード
        print key0_neighbors

        keylist2=list(keylist)
        for j in range(n_num):
                keylist2.remove(key0_neighbors[j])


        key2_0_neighbors=list(g.neighbors(int(keylist2[1])))
        print keylist2
        #keylistの0番目の数字に隣接していないノードのリスト
        print key2_0_neighbors
        #keylist2の１番目の数字に隣接しているノードのリスト

        old_set = set(keylist2)
        new_set = set(key2_0_neighbors)
        m_set = set(main_keylist)
        old_new_set = old_set-new_set
        good_vertex =m_set & old_new_set
        len_good_vertex = len(good_vertex)# 削除された個数

        print good_vertex
        print len_good_vertex

        c0_list=list(good_vertex)


        for m in range(len_good_vertex):
                main_keylist.remove(c0_list[m])


        print main_keylist

        for c0 in range(len_good_vertex):
                dict2[c0_list[c0]]=clist[c_num]

        print dict2

        if len(c0_list)==0:
                break

print "minimum_number_color : "+ str(c_num+1)


