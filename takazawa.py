#coding: UTF-8
# Python 2.7.6
# B3 Eiichi Takazawa
import networkx as nx
import matplotlib.pyplot as plt
import sys
import time
import random
import numpy as np

# 支配集合になっているか確認 
def is_dominating(g,bitset):
	dominated = set([])	
	for u in range(len(g.nodes())):
		if bitset & (1<<u):			# そのbitが立っていれば
			dominated.add(u)		# 集合に追加
			for v in g.neighbors(u):	# 隣接ノードも追加
				dominated.add(v)

	# 支配した集合の要素数と全ノード数が一致していれば被覆しているといえる
	if len(dominated) == len(g.nodes()):
		return True	
	else:
		return False

class DominatingSet(object):
	def __init__(self,g):
		self.answer = sys.maxint
		self.V = len(g.nodes())
		self.neighbor = [0] * self.V
		self.g = g

		for u in g.nodes(): # 隣接頂点のビットパターンを前計算
			self.neighbor[u] |= (1 << u)
			for v in g.neighbors(u):
				self.neighbor[u] |= (1 << v)

		# 次数を降順でソート
		self.g_deg = [[key,g.degree(key)] for key in g.degree()]
		self.g_deg = sorted(self.g_deg,key=lambda x:x[1],reverse=True)
		self.ord_nodes = [x[0] for x in self.g_deg]

		# それ以降の頂点で被覆できる頂点
		self.cover_nodes = [0] * self.V
		tmp = 0
		for v in reversed(self.ord_nodes):
			tmp |= v | self.neighbor[v]
			self.cover_nodes[v] |= tmp

		# 必ず支配集合に含まれる頂点
		self.is_add = [False] * self.V
		for u in g.nodes():
			if g.degree(u) == 0:
				is_add[u] = True
			for v in g.neighbors(u):
				if g.degree(v) == 1:
					is_add[u] = True
					break

	def solve(self):
		self.answer = sys.maxint
		self.ansset = 0
		self.dfs(0,0,0,0)
		ans = []	# 支配集合のリスト
		for i in range(self.V):
			if self.ansset & (1<<i):
				ans.append(i)
		return ans

	# n:幾つ目の頂点,bit:被覆済頂点集合,add:支配集合,cost:使った頂点数
	# DFS + 枝刈り
	def dfs(self,n,bitset,addset,cost):
		if bitset == (1<<self.V)-1: # 全ての頂点を被覆できたので更新
			if self.answer > cost:
				self.answer = cost
				self.ansset = addset
			return

		if n == self.V: # 最後まで探索したので終了
			return
		if cost > self.answer: # 最小ではないので終了
			return

		v = self.ord_nodes[n]	# 次数の大きい順に追加
		if (bitset | self.cover_nodes[v]) != (1<<self.V)-1:	# その後の頂点で被覆不可能
			return

		if self.is_add[v]:
			self.dfs(n+1, bitset|self.neighbor[v],addset|(1<<v),cost+1)
		else:	
			self.dfs(n+1, bitset|self.neighbor[v],addset|(1<<v),cost+1)
			self.dfs(n+1,bitset,addset,cost)


# 全パターンを貪欲に探索する O(2^N)
def search_dominating(g):
	res = sys.maxint	# 最小の要素数
	ansset = 0			# 支配集合
	for bitset in range(1,1<<len(g.nodes())):	#全パターンを探索
		if is_dominating(g,bitset):
			if res > bin(bitset).count("1"):	# 集合の要素数を更新
				res = bin(bitset).count("1")
				ansset = bitset

	ans = []	# 支配集合のリスト
	for i in range(len(g.nodes())):
		if ansset & (1<<i):
			ans.append(i)
	return ans

# newman watts strogatz graphの
# ノード数を変化させた時の支配集合を求め時間を計測する
def TestNumNode(fr,to):
	x1 = []
	x2 = []
	y1 = []
	y2 = []
	# ノード数を4~変化させる
	for t in range(fr,to+1):
		print t
		g = nx.newman_watts_strogatz_graph(t,3,0.5,seed=1)
		ds = DominatingSet(g)

		tm1 = time.clock()
		if t < 21:
			domSet1 = search_dominating(g)
			tm2 = time.clock()
			y1.append(tm2-tm1)
			x1.append(t)

		tm1 = time.clock()
		domSet2 = ds.solve()
		tm2 = time.clock()
		y2.append(tm2-tm1)
		x2.append(t)

	print y1,y2
	plt.plot(x1,y1,color="g",marker="o",label="Full Search")
	plt.plot(x2,y2,color="b",marker="o",label="DFS+Pruning")
	plt.xlabel('The number of Nodes')
	plt.ylabel('time[s]')
	plt.legend(loc="upper left")
	plt.ylim(0,25)
	plt.xlim(0,to)
	plt.show()


# ノード数を4~変化させた時の計算時間折れ線グラフの表示
TestNumNode(4,33)

# dominating set の一例表示
g = nx.newman_watts_strogatz_graph(30,3,0.8)
domSet = DominatingSet(g).solve()
# print nx.is_dominating_set(g,domSet)

pos = nx.circular_layout(g)
nx.draw_networkx_nodes(g,pos,nodelist=g.nodes()) #,node_size=600)
nx.draw_networkx_edges(g,pos,edgelist=g.edges())
nx.draw_networkx_nodes(g,pos,nodelist=domSet,node_color='b')#,alpha=0.8)
plt.axis('off')
plt.show()

